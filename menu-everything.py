import argparse
import shlex
import sys
from typing import Dict

import plugins
from dmenu import DMenu


def main(
        arguments: argparse.Namespace,
        available_plugins: Dict[str, plugins.Plugin] = None
) -> int:
    if available_plugins is None:
        available_plugins = {}
    user_args = vars(arguments)
    selected_plugin_name: str = user_args['mode']

    selected_plugin: plugins.Plugin
    try:
        selected_plugin = available_plugins[selected_plugin_name]
    except KeyError:
        selected_plugin = plugins.default_plugin

    dmenu = DMenu(**user_args)
    return selected_plugin.run(dmenu=dmenu, **user_args)


if __name__ == '__main__':
    loaded_plugins = plugins.load()
    plugin_choices = loaded_plugins.keys()

    parser = argparse.ArgumentParser(
        description='A dmenu (or other) wrapper that can perform a limitless array of functions',
        add_help=True,
        conflict_handler='resolve'
    )
    parser.add_argument(
        'mode',
        metavar='mode',
        nargs='?',
        choices=plugin_choices,
        default='echo'
    )
    parser.add_argument(
        '--dmenu-command',
        dest='dmenu_command',
        type=lambda s: shlex.split(s),
        default=['dmenu', '-i']
    )

    for plugin in loaded_plugins.values():
        plugin.add_args(parser)

    args = parser.parse_args()
    status = main(args, available_plugins=loaded_plugins)
    sys.exit(status)
