from typing import Any, DefaultDict, Dict

from collections import defaultdict
from common import BaseObject


class CacheUser(BaseObject):
    def __init__(self, *args, cache: DefaultDict[str, Dict[str, Any]] = None, **kwargs):
        super().__init__(*args, **kwargs)
        if cache is None:
            cache = defaultdict(lambda: {})
        self._cache: Dict[str, Dict[str, Any]] = cache

    @property
    def cache(self) -> Dict[str, Dict[str, Any]]:
        return self._cache
