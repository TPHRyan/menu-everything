import os
import shlex
import stat
import subprocess
from typing import List

_TRY_PATHS: List[str] = [
    '/usr/bin',
    '/usr/local/bin',
    '/bin',
]

_SENSIBLE_TERMINAL_LIST: List[List[str]] = [
    ['rofi-sensible-terminal', '-e'],
    [os.getenv('TERMINAL'), '-e'],
    ['x-terminal-emulator', '-e'],
    ['termite', '-e'],
    ['urxvt', '-e'],
    ['rxvt', '-e'],
    ['xst', '-e'],
    ['terminator', '-e'],
    ['Eterm', '-e'],
    ['aterm', '-e'],
    ['xterm', '-e'],
    ['gnome-terminal', '-e'],
    ['roxterm', '-e'],
    ['xfce4-terminal', '-e'],
]


def launch_sensible_terminal(
        command: str = None,
        *,
        provided_terminal: List[str] = None
) -> subprocess.CompletedProcess:
    if provided_terminal is not None:
        found_terminal = provided_terminal
    else:
        found_terminal = ['/usr/bin/xterm', '-e']
        found_non_default_terminal = False
        for terminal in _SENSIBLE_TERMINAL_LIST:
            if found_non_default_terminal:
                break
            for path in _TRY_PATHS:
                full_terminal_path = os.path.join(path, terminal[0])
                if not os.path.isfile(full_terminal_path):
                    continue
                file_mode = os.stat(full_terminal_path).st_mode
                if file_mode & (stat.S_IEXEC | stat.S_IXGRP | stat.S_IXOTH):
                    found_terminal = [full_terminal_path] + terminal[1:]
                    found_non_default_terminal = True
                    break
    command = shlex.split(command)
    full_command = found_terminal + command
    return subprocess.run(
        full_command,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        text=True
    )
