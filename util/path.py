import os
import sys
from typing import List


def find_in_path(mode: int) -> List[str]:
    script_path = os.path.abspath(os.path.dirname(sys.argv[0]))
    path = os.getenv('PATH')
    for path_dir in path.split(':'):
        if os.path.commonpath([script_path, os.path.abspath(path_dir)]) == script_path:
            continue
        for filename in os.listdir(path_dir):
            full_path = os.path.join(path_dir, filename)
            if os.path.isfile(full_path):
                file_mode = os.stat(full_path).st_mode
                if mode & file_mode:
                    yield full_path
