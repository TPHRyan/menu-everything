from typing import NamedTuple


class MenuItem(NamedTuple):
    label: str
    action: str


class BaseObject(object):
    def __init__(self, *args, **kwargs):
        super().__init__()
