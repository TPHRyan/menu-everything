import argparse
from abc import ABC, abstractmethod

from common import BaseObject


class ArgInjector(ABC, BaseObject):
    @abstractmethod
    def add_args(self, parser: argparse.ArgumentParser):
        pass


class ArgInjectorStatic(ABC, BaseObject):
    @classmethod
    @abstractmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        pass
