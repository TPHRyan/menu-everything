import subprocess
from typing import Iterable, List, Optional

from common import MenuItem, BaseObject


class DMenu(BaseObject):
    def __init__(self, *, dmenu_command=None, **kwargs):
        super().__init__(**kwargs)
        if dmenu_command is None:
            dmenu_command = ['dmenu', '-i']
        self.dmenu_command: List[str] = dmenu_command
        self.items: List[MenuItem] = []

    def get_user_choice(self) -> Optional[str]:
        process: subprocess.Popen = subprocess.Popen(
            self.dmenu_command,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )

        input_string = '\n'.join(item.label for item in self.items)
        choice: str
        choice, stderr = process.communicate(input_string)
        if process.returncode and process.returncode != 0:
            if stderr == '':
                return None
            else:
                raise RuntimeError(f'Dmenu error: {stderr}')
        else:
            return choice.strip('\n')

    def add_item(self, menu_item: MenuItem):
        self.items.append(menu_item)

    def add_items(self, menu_items: Iterable[MenuItem]):
        for item in menu_items:
            self.add_item(item)
