from abc import abstractmethod

from common import MenuItem
from stages.base import BaseStage, StageRole


class ActionHandler(BaseStage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def role(self) -> StageRole:
        return StageRole.Action

    @abstractmethod
    def run(self, action: MenuItem, **kwargs) -> int:
        return 0
