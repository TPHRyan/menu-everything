import argparse
import os
import shlex
import subprocess
from typing import List, Optional

import util.sensibleterminal as sensibleterminal
from stages.input import DesktopFile
from common import MenuItem
from .fork import ForkingActionHandler


class ApplicationActionHandler(ForkingActionHandler):
    def __init__(self, *args, terminal_command: Optional[List[str]] = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.terminal_command: Optional[List[str]] = terminal_command

    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            '--terminal-command',
            dest='terminal_command',
            type=lambda s: shlex.split(s),
            default=None
        )

    def run_as_child(self, action: MenuItem) -> int:
        desktop_file: DesktopFile = self.cache['desktop_entry'][action.action]
        if desktop_file.path is not None:
            os.chdir(desktop_file.path)
        if desktop_file.terminal:
            completed_command = sensibleterminal.launch_sensible_terminal(
                command=desktop_file.exec,
                provided_terminal=self.terminal_command
            )
            print(completed_command.stdout)
            return completed_command.returncode
        else:
            completed_command = subprocess.run(
                shlex.split(desktop_file.exec),
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE,
                text=True
            )
            print(completed_command.stdout)
            return completed_command.returncode
