import argparse
import shlex
import subprocess

from common import MenuItem
from .fork import ForkingActionHandler


class ShellActionHandler(ForkingActionHandler):
    def __init__(self, *args, shell_command=None, **kwargs):
        super().__init__(*args, **kwargs)
        if shell_command is None:
            shell_command = ['/bin/bash', '-e']
        self.shell_command = shell_command

    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            '--shell-command',
            dest='shell_command',
            type=lambda s: shlex.split(s),
            default=['/bin/bash', '-c']
        )

    def run_as_child(self, action: MenuItem) -> int:
        full_command = self.shell_command + shlex.split(action.action)
        completed_command = subprocess.run(
            full_command,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
            text=True
        )
        print(completed_command.stdout)
        return completed_command.returncode
