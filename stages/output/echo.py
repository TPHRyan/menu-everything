import argparse

from .base import ActionHandler
from common import MenuItem


class EchoActionHandler(ActionHandler):
    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        pass

    def run(self, action: MenuItem, **kwargs) -> int:
        print(f'{action.label}: {action.action}')
        return 0
