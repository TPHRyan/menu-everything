import argparse
import os
import sys

from .base import ActionHandler
from common import MenuItem


class ForkingActionHandler(ActionHandler):
    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        pass

    def run(self, action: MenuItem, **kwargs) -> int:
        try:
            pid = os.fork()
            if pid > 0:
                return self.run_as_parent(action)
        except OSError as e:
            print(
                f'Forking action failed after menu selection: {e.errno} ({e.strerror})',
                file=sys.stderr
            )
            sys.exit(1)

        os.setsid()
        try:
            pid = os.fork()
            if pid > 0:
                # Second parent, not processing
                sys.exit(0)
        except OSError as e:
            print(
                f'Second forking action failed after menu selection: {e.errno} ({e.strerror})',
                file=sys.stderr
            )
            sys.exit(1)

        return self.run_as_child(action)

    def run_as_child(self, action: MenuItem) -> int:
        return 0

    def run_as_parent(self, action: MenuItem) -> int:
        return 0
