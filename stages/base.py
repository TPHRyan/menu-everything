import argparse
from abc import abstractmethod
from enum import Enum, auto
from typing import Any

from arginject import ArgInjectorStatic
from util.cache import CacheUser


class StageRole(Enum):
    Menu = auto()
    Action = auto()


class BaseStage(ArgInjectorStatic, CacheUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        pass

    @property
    @abstractmethod
    def role(self) -> StageRole:
        pass

    @abstractmethod
    def run(self, *args, **kwargs) -> Any:
        pass
