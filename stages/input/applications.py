import configparser
import os
import re
from typing import Generator, List, Optional

from .base import MenuItemGenerator, MenuItem
from common import BaseObject


class ApplicationMenuItemGenerator(MenuItemGenerator):
    FILE_EXTENSION = '.desktop'
    FILE_EXTENSION_LENGTH = len(FILE_EXTENSION)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self, **kwargs) -> Generator[MenuItem, None, None]:
        search_dirs = self.get_search_dirs()
        for search_dir in search_dirs:
            if not os.path.isdir(search_dir):
                continue
            for filename in os.listdir(search_dir):
                if (len(filename) < self.FILE_EXTENSION_LENGTH or
                        filename[-self.FILE_EXTENSION_LENGTH:] != self.FILE_EXTENSION):
                    continue
                full_path = os.path.join(search_dir, filename)
                if os.path.isfile(full_path):
                    if filename not in self.cache['desktop_entry']:
                        try:
                            desktop_file = DesktopFile(full_path)
                            self.cache['desktop_entry'][filename] = desktop_file
                        except PermissionError:
                            continue
                        yield MenuItem(label=desktop_file.name, action=filename)

    def get_search_dirs(self) -> List[str]:
        def add_dir(d: str, l: List[str]):
            d = d.rstrip('/')
            if len(d) < 13 or d[-13:] != '/applications':
                d += '/applications'
            l.append(d)
        search_dirs: List[str] = []
        user_desktops_folder = os.getenv('XDG_DATA_HOME')
        if user_desktops_folder is None:
            user_desktops_folder = os.path.join(os.getenv('HOME'), '.local', 'share')
        add_dir(user_desktops_folder, search_dirs)

        xdg_data_dirs_env = os.getenv('XDG_DATA_DIRS')
        if xdg_data_dirs_env is None:
            xdg_data_dirs = [
                '/usr/share',
                '/usr/local/share'
            ]
        else:
            xdg_data_dirs = xdg_data_dirs_env.split(':')
        for data_dir in xdg_data_dirs:
            add_dir(data_dir, search_dirs)
        return search_dirs


class DesktopFile(BaseObject):
    MAIN_SECTION = 'Desktop Entry'

    def __init__(self, filepath):
        super().__init__()
        self.config = configparser.ConfigParser(
            interpolation=IgnoreFieldCodesInterpolation()
        )
        with open(filepath, 'r') as f:
            self.filename = os.path.basename(filepath)
            self.config.read_file(f)

    @property
    def name(self) -> str:
        return self._get_desktop_key('Name', self.filename)

    @property
    def generic_name(self) -> str:
        return self._get_desktop_key('GenericName', self.name)

    @property
    def comment(self) -> str:
        return self._get_desktop_key('Comment', self.generic_name)

    @property
    def path(self) -> Optional[str]:
        return self._get_desktop_key('Path')

    @property
    def exec(self) -> Optional[str]:
        return self._get_desktop_key('Exec')

    @property
    def terminal(self) -> bool:
        return True if self._get_desktop_key('Terminal') == 'true' else False

    def _get_desktop_entry_section(self) -> configparser.SectionProxy:
        return self.config[self.MAIN_SECTION]

    def _get_desktop_key(self, key, default=None) -> str:
        try:
            return self._get_desktop_entry_section()[key]
        except KeyError:
            return default


class IgnoreFieldCodesInterpolation(configparser.Interpolation):
    def __init__(self):
        self.regex_ignore = re.compile(r'\s?%[a-zA-Z]')

    def before_read(self, parser, section, option, value):
        return self.regex_ignore.sub('', value)
