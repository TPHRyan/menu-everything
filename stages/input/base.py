import argparse
from typing import Generator

from common import MenuItem
from stages.base import BaseStage, StageRole


class MenuItemGenerator(BaseStage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.items = []

    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        pass

    @property
    def role(self) -> StageRole:
        return StageRole.Menu

    def run(self, *args, **kwargs) -> Generator[MenuItem, None, None]:
        for item in self.items:
            yield item
