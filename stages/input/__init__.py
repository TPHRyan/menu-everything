import stages.input.base

from .applications import ApplicationMenuItemGenerator
from .applications import DesktopFile
from .binaries import BinaryMenuItemGenerator
from .const import ConstantMenuGenerator
