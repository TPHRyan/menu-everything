import argparse
from typing import List

from .base import MenuItemGenerator
from common import MenuItem


class ConstantMenuGenerator(MenuItemGenerator):
    def __init__(self, menu_items: List[MenuItem] = None, **kwargs):
        super().__init__(**kwargs)
        if menu_items is None:
            menu_items = []
        self.items: List[MenuItem] = menu_items

    @classmethod
    def add_args(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            '--menu-items',
            type=lambda s: s.split(','),
            dest='menu_items',
            required=False
        )
