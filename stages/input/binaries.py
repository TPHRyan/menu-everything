import os
import shutil
import stat
import subprocess
import util
from typing import Generator

from .base import MenuItemGenerator, MenuItem


class BinaryMenuItemGenerator(MenuItemGenerator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ItemGenerator = Generator[MenuItem, None, None]

    def run(self, **kwargs) -> ItemGenerator:
        dmenu_path = shutil.which('dmenu_path')
        if dmenu_path is not None:
            return self.fetch_menu_items_dmenu(dmenu_path)
        else:
            return self.fetch_menu_items_fallback()

    def fetch_menu_items_dmenu(self, dmenu_path: str) -> ItemGenerator:
        try:
            path_result = subprocess.run(
                dmenu_path,
                timeout=10,
                stdout=subprocess.PIPE,
                stderr=subprocess.DEVNULL,
                text=True
            )
            if path_result.returncode != 0:
                print('Failed to fetch entries using dmenu, using fallback...')
                yield from self.fetch_menu_items_fallback()
            for item in path_result.stdout.split('\n'):
                yield item

        except subprocess.TimeoutExpired:
            print('Failed to fetch entries using dmenu, using fallback...')
            yield from self.fetch_menu_items_fallback()

    def fetch_menu_items_fallback(self) -> ItemGenerator:
        for filepath in util.path.find_in_path(stat.S_IEXEC | stat.S_IXGRP | stat.S_IXOTH):
            yield MenuItem(label=os.path.basename(filepath), action=filepath)
