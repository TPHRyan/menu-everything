import argparse
import bisect
from typing import Callable, Dict, List, Optional, Type

from arginject import ArgInjector
from common import MenuItem, BaseObject
from dmenu import DMenu
from stages.base import BaseStage, StageRole


class Plugin(ArgInjector, BaseObject):
    def __init__(
            self,
            *stage_classes: Type[BaseStage],
            add_args: Optional[Callable[[argparse.ArgumentParser], None]] = None,
            **kwargs
    ):
        super().__init__(*stage_classes, **kwargs)
        self.stage_classes = stage_classes
        self.add_args_fn = add_args

    def add_args(self, parser: argparse.ArgumentParser):
        for stage_class in self.stage_classes:
            stage_class.add_args(parser)
        if self.add_args_fn is not None:
            self.add_args_fn(parser)

    def run(self, *args, dmenu: DMenu, **kwargs) -> int:
        run_input = ()
        for stage_class in self.stage_classes:
            stage = stage_class(*args, **kwargs)
            role = stage.role
            if role == StageRole.Menu:
                item_lookup: Dict[str, MenuItem] = {}
                sorted_items: List[MenuItem] = []
                # Sort the items while also creating a map to access the items from user choice
                for item in stage.run(*run_input, **kwargs):
                    item: MenuItem
                    if item.label not in item_lookup:
                        item_lookup[item.label] = item
                        bisect.insort_left(sorted_items, item)

                dmenu.add_items(menu_items=sorted_items)
                choice = dmenu.get_user_choice()
                if choice is None:
                    return 1
                kwargs['menu_item_lookup'] = item_lookup
                try:
                    run_input = (item_lookup[choice],)
                except KeyError:
                    return 1
            elif role == StageRole.Action:
                output_status = stage.run(*run_input, **kwargs)
                if output_status != 0:
                    return output_status
            else:
                stage.run(*args, **kwargs)
            kwargs['cache'] = stage.cache
        return 0

