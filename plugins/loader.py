from typing import Dict

from .plugin import Plugin
from stages.input import ApplicationMenuItemGenerator, BinaryMenuItemGenerator, ConstantMenuGenerator
from stages.output import ApplicationActionHandler, EchoActionHandler, ShellActionHandler


default_plugin = Plugin(
    ConstantMenuGenerator,
    EchoActionHandler
)

default_plugins = {
    'application': Plugin(
        ApplicationMenuItemGenerator,
        ApplicationActionHandler
    ),
    'echo': default_plugin,
    'shell': Plugin(
        BinaryMenuItemGenerator,
        ShellActionHandler
    )
}


def load() -> Dict[str, Plugin]:
    return default_plugins.copy()
